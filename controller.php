<?php
session_start();

include "model.php";

class controller{
    public $model;

    public function __construct()
    {
        $this->model = new model();
    }


    public function hra()
    {
        if ($_COOKIE["1"] & $_COOKIE["2"] & $_COOKIE["3"] & $_COOKIE["4"] ){
            include 'View/konec.php';
        }
        else {
            if ($_SESSION["hra"]) {
                if ($_SESSION["misto"]) //session má hodnotu
                {
                    //zobrazí jeden určitý úkol
                    $misto = $this->model->ukazMisto($_SESSION['misto']);
                    include 'View/ukol.php';
                } elseif ($_GET['misto']) {
                    // zobrazí jeden úkolu
                    $_SESSION['misto'] = $_GET['misto'];
                    $misto = $this->model->ukazMisto($_SESSION['misto']);
                    include 'View/ukol.php';
                } else { //zobrazí se seznam úkolů
                    $mista = $this->model->ukazMista();
                    $mapa = $this->model->mapa();
                    include 'View/mapa.php';
                }
            } //zobrazení úvodní obrazovky
            else {
                $uvodni = $this->model->uvodniText();
                $informace = $this->model->informace();
                $video = $this->model->video();
                include 'View/uvodni.php';

            }
        }
    }

    public function navratUvod(){
        unset($_SESSION['hra']);

        setcookie("1", '', time()-3600, '/');
        setcookie("2", '', time()-3600, '/');
        setcookie("3", '', time()-3600, '/');
        setcookie("4", '', time()-3600, '/');

        header("location:index.php");
    }

    public function navratMapa(){
        unset($_SESSION["misto"]);
        header ("location:index.php");
    }

    public function kontrola(){
        $odpoved = $_POST ['odpoved'];
        $spravna= $this->model->ukazMisto($_SESSION ["misto"]);

        if (empty($odpoved)){
            echo "<script>alert('Nic nebylo napsáno')</script>";
        } else {
            if ($odpoved === $spravna->odpoved){
                setcookie($spravna->id,"ok",time() + (86300*30), "/" );
                echo "<script>alert('Správná odpověď!')</script>";
                unset($_SESSION["misto"]);
                header("Refresh: 0; URL=index.php");}


            else{
                echo "<script>alert('Odpověď není správná!')</script>";
            }
        }
    }
}