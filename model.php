<?php
include ("Class/misto.php");

class model{
    public function ukazMista(){
        return array(
            "Skotel" => new misto("1", "Skotel", "multimedia", "Soubory/skotel.mp4"),
            "Číselné sklepení" => new misto("2", "Číselné sklepení", "matematika", "Soubory/sklepeni.mp4"),
            "Hostinec s křížem" => new misto("3", "Hostinec s křížem", "polsko", "Soubory/hostinec.mp4"),
            "Maják" => new misto ("4", "Maják", "navigace", "Soubory/majak.mp4")
        );
    }

    public function ukazMisto ($nazev){
        $vsechnyMista = $this->ukazMista();
        return $vsechnyMista[$nazev];
    }

    public function uvodniText(){
        return(
            "Vítejte před branami městačká Záhadov! Jak již název napovídá, městočko je plné různých šifer a hádanek. 
            Pomůžete hlavnímu protagonistovi tohoto příběhu vyřešit všechny hádanky a další nástrahy, které ho v Záhadově čekají?
            ?"
        );
    }

    public function informace(){
            return(
                "Autor: Jakub Vaněček"."</br>".
                "Tato webové hra byla vytvořena jako práce na předměty Tvorba a Design Multimediálních Aplikací a Tvorba Webovách Aplikací"
            );
    }

    public function video(){
            return(
                "Soubory/uvod.mp4"
            );
    }

    public function mapa(){
            return (
                "Soubory/mapa.jpg"
            );
    }
}